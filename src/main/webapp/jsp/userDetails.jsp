<%@ include file="/jsp/jspf/page.jspf" %>
<%@ include file="/jsp/jspf/include.jspf" %>

<%@ include file="/jsp/jspf/head.jspf" %>

<body>

<div class="container">

    <h2 class="text-center">
        <bean:message key="label.userDetails.heading"/>
    </h2>
    <p class="text-center">
        <bean:message key="label.userDetails.headingCaption"/>
    </p>

    <table class="table">
        <tbody>
            <tr>
                <th scope="row"><bean:message key="label.user.firstName"/></th>
                <td><bean:write name="userForm" property="firstName" /></td>
            </tr>
            <tr>
                <th scope="row"><bean:message key="label.user.lastName"/></th>
                <td><bean:write name="userForm" property="lastName" /></td>
            </tr>
            <tr>
                <th scope="row"><bean:message key="label.user.email"/></th>
                <td><bean:write name="userForm" property="email" /></td>
            </tr>
            <tr>
                <th scope="row"><bean:message key="label.user.gender"/></th>
                <td><bean:write name="userForm" property="genderName" /></td>
            </tr>
            <tr>
                <th scope="row"><bean:message key="label.user.age"/></th>
                <td><bean:write name="userForm" property="age" /></td>
            </tr>
            <tr>
                <th scope="row"><bean:message key="label.user.balance"/></th>
                <td><bean:write name="userForm" property="balance" format="0.0000"/></td>
            </tr>
        </tbody>
    </table>

    <div class="row p-1">
        <div class="col-sm-6 col-lg-2">
            <html:link action="/createEdit" paramId="userId" paramName="userForm" paramProperty="userId" styleClass="btn btn-secondary"><bean:message key="btn.user.update"/></html:link>
        </div>
        <div class="col-sm-6 col-lg-2">
            <html:link action="/showUserList" styleClass="btn btn-secondary"><bean:message key="btn.show.userList"/></html:link>
        </div>
    </div>

</div>

<%@ include file="/jsp/jspf/footer.jspf" %>

</body>
</html>
