<%@ include file="/jsp/jspf/page.jspf" %>
<%@ include file="/jsp/jspf/include.jspf" %>

<%@ include file="/jsp/jspf/head.jspf" %>

<body>

<div class="container">

    <div class="row">
        <h1 class="text-center"><bean:message key="label.userListing.title"/></h1>
    </div>


    <div class="table-responsive">
        <display:table id="user" name="usersList" requestURI="/showUserList.do" sort="list" defaultsort="1" pagesize="10" varTotals="totals" class="table table-striped" decorator="org.displaytag.decorator.TotalTableDecorator">
            <display:column property="userId" titleKey="label.user.id"/>
            <display:column property="firstName" titleKey="label.user.firstName" sortable="true"/>
            <display:column property="lastName" titleKey="label.user.lastName" sortable="true"/>
            <display:column property="email" titleKey="label.user.email" sortable="true"/>
            <display:column property="age" format="{0}" titleKey="label.user.age" sortable="true"/>
            <display:column property="genderName" titleKey="label.user.gender" sortable="true"/>
            <display:column property="balance" format="{0,number,0.00}" titleKey="label.user.balance" sortable="true" total="true" />
            <display:column titleKey="btn.user.details" url="/lookUpUser.do" paramId="userId" paramProperty="userId">
                <bean:message key="btn.user.details"/>
            </display:column>
            <display:column titleKey="btn.user.delete" url="/deleteUser.do" paramId="userId" paramProperty="userId">
                <bean:message key="btn.user.delete"/>
          </display:column>
        </display:table>
    </div>


    <div class="row p-1">
        <html:link action="/createEdit" styleClass="btn btn-secondary"><bean:message key="btn.user.create"/></html:link>
    </div>

    <div class="row p-1">
        <div class="col-md-12 col-lg-10">
            <h5 class="font-weight-bold"><bean:message key="label.total.user.balance"/> : <bean:write name="totals" property="column7" format="0.00"/> UAH </h5>
        </div>
    </div>
</div>

<%@ include file="/jsp/jspf/footer.jspf" %>
</body>
</html>
