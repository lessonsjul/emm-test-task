<%@ include file="/jsp/jspf/page.jspf" %>
<%@ include file="/jsp/jspf/include.jspf" %>

<%@ include file="/jsp/jspf/head.jspf" %>

<body>

<div class="container">

    <logic:empty name="userForm" property="userId">
        <h2 class="text-center">
            <bean:message key="label.userCreate.heading"/>
        </h2>
    </logic:empty>
    <logic:notEmpty name="userForm" property="userId">
        <h2 class="text-center">
            <bean:message key="label.userEdit.heading"/>
        </h2>
    </logic:notEmpty>
    <p class="text-center">
        <bean:message key="label.userCreateEdit.headingCaption"/>
    </p>

    <div class="row">
        <div class="col-md-12 col-lg-10 m-auto">

            <html:form action="/process-user" method="post" target="userForm">
                <logic:notEmpty name="userForm" property="userId">
                    <html:hidden name="userForm" property="userId"/>
                </logic:notEmpty>

            <div class="form-group">
                <logic:messagesPresent property="firstName">
                    <div class="alert alert-danger"><html:errors property="firstName"/></div>
                </logic:messagesPresent>
                <label class="col-sm-2 col-form-label">
                    <bean:message key="label.user.firstName"/>
                </label>
                <div class="col-sm-10">
                    <html:text name="userForm" property="firstName" errorStyleClass="has-error" styleClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <logic:messagesPresent property="lastName">
                    <div class="alert alert-danger"><html:errors property="lastName"/></div>
                </logic:messagesPresent>
                <label class="col-sm-2 col-form-label">
                    <bean:message key="label.user.lastName"/>
                </label>
                <div class="col-sm-10">
                    <html:text name="userForm" property="lastName" errorStyleClass="has-error" styleClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <logic:messagesPresent property="email">
                    <div class="alert alert-danger"><html:errors property="email"/></div>
                </logic:messagesPresent>
                <label class="col-sm-2 col-form-label">
                    <bean:message key="label.user.email"/>
                </label>
                <div class="col-sm-10">
                    <html:text name="userForm" property="email" errorStyleClass="has-error" styleClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <logic:messagesPresent property="birthDate">
                    <div class="alert alert-danger"><html:errors property="birthDate"/></div>
                </logic:messagesPresent>
                <logic:messagesPresent property="dateFormat">
                    <div class="alert alert-danger"><html:errors property="dateFormat"/></div>
                </logic:messagesPresent>
                <logic:messagesPresent property="age">
                    <div class="alert alert-danger"><html:errors property="age"/></div>
                </logic:messagesPresent>
                <label class="col-sm-2 col-form-label">
                    <bean:message key="label.user.bithdate"/>
                </label>
                <div class="col-sm-10">
                    <html:text name="userForm" property="birthDate" styleId="datepicker" errorStyleClass="has-error" styleClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <logic:messagesPresent property="balance">
                    <div class="alert alert-danger"><html:errors property="balance"/></div>
                </logic:messagesPresent>
                <logic:messagesPresent property="balanceFormat">
                    <div class="alert alert-danger"><html:errors property="balanceFormat"/></div>
                </logic:messagesPresent>

                <label class="col-sm-2 col-form-label">
                    <bean:message key="label.user.balance"/>
                </label>
                <div class="col-sm-10">
                    <html:text name="userForm" property="balanceInput" errorStyleClass="has-error" styleClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <logic:messagesPresent property="gender">
                    <div class="alert alert-danger"><html:errors property="gender"/></div>
                </logic:messagesPresent>
                <label class="col-sm-2 col-form-label">
                    <bean:message key="label.user.gender"/>
                </label>
                <div class="col-sm-10">
                    <html:select name="userForm" property="genderId" errorStyleClass="has-error" styleClass="custom-select col-sm-4 small">
                        <html:option value="-1">
                            Select any Category
                        </html:option>
                        <logic:present name="genderList">
                            <html:optionsCollection name="genderList" label="name" value="id"/>
                        </logic:present>
                    </html:select>
                </div>
            </div>

                <html:submit styleClass="btn btn-primary"><bean:message key="btn.user.save"/></html:submit>
            </html:form>
        </div>
    </div>

    <div class="row p-1">
        <html:link action="/showUserList" styleClass="btn btn-secondary"><bean:message key="btn.show.userList"/></html:link>
    </div>


</div>

<%@ include file="/jsp/jspf/footer.jspf" %>

</body>
</html>
