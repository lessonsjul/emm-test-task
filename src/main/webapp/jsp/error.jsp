<%@ include file="/jsp/jspf/page.jspf" %>
<%@ include file="/jsp/jspf/include.jspf" %>

<%@ include file="/jsp/jspf/head.jspf" %>
<%@ page isErrorPage="true" %>
<body>

<div class="container">

    <div class="row">
        <div class="col-md-12 col-lg-10">
            <h1>Oops!</h1>
            <bean:message key="error.global.mesage"/>

            <%= exception %>

        </div>
    </div>


<%@ include file="/jsp/jspf/footer.jspf" %>
</div>
</body>
</html>