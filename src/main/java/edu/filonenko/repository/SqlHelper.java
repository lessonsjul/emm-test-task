package edu.filonenko.repository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Yulia Filonenko
 **/
class SqlHelper {

    public static final String ID_FIELD_NAME = "id";
    public static final String FIRST_NAME_FIELD_NAME = "first_name";
    public static final String LAST_NAME_FIELD_NAME = "last_name";
    public static final String EMAIL_FIELD_NAME = "email";
    public static final String BIRTH_DATE_FIELD_NAME = "birth_date";
    public static final String GENDER_FIELD_NAME = "gender";
    public static final String BALANCE_FIELD_NAME = "balance";

    private static final List<String> ROW_MAPPER_FIELDS =
            Arrays.asList(FIRST_NAME_FIELD_NAME, LAST_NAME_FIELD_NAME, EMAIL_FIELD_NAME, BIRTH_DATE_FIELD_NAME, GENDER_FIELD_NAME, BALANCE_FIELD_NAME);

    private static final String LIST_OF_FIELDS_NAME =
            ROW_MAPPER_FIELDS.stream().collect(Collectors.joining(", "));

    private static final String LIST_OF_FIELDS_NAME_FOR_UPDATE =
            ROW_MAPPER_FIELDS.stream().collect(Collectors.joining(" = ?, ", "", " = ?"));

    private static final String USER_TABLE_NAME = "Users";

    public static final String CREATE_USER_SQL = "insert into " + USER_TABLE_NAME + " (" + LIST_OF_FIELDS_NAME + ") values (?, ?, ?, ?, ?, ?)";
    public static final String UPDATE_USER_SQL = "update " + USER_TABLE_NAME + " set " + LIST_OF_FIELDS_NAME_FOR_UPDATE + " where " + ID_FIELD_NAME + " = ?";
    public static final String SELECT_USERS_SQL = "select " + ID_FIELD_NAME + ", " + LIST_OF_FIELDS_NAME + " from " + USER_TABLE_NAME;
    public static final String SELECT_USER_BY_ID_SQL = SELECT_USERS_SQL + " where " + ID_FIELD_NAME + " = ?";
    public static final String DELETE_USER_BY_ID_SQL = "delete from " + USER_TABLE_NAME + " where " + ID_FIELD_NAME + " = ?";
}
