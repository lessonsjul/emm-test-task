package edu.filonenko.repository;

import edu.filonenko.DbException;
import edu.filonenko.entity.Gender;
import edu.filonenko.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

import static edu.filonenko.repository.SqlHelper.BALANCE_FIELD_NAME;
import static edu.filonenko.repository.SqlHelper.BIRTH_DATE_FIELD_NAME;
import static edu.filonenko.repository.SqlHelper.CREATE_USER_SQL;
import static edu.filonenko.repository.SqlHelper.DELETE_USER_BY_ID_SQL;
import static edu.filonenko.repository.SqlHelper.EMAIL_FIELD_NAME;
import static edu.filonenko.repository.SqlHelper.FIRST_NAME_FIELD_NAME;
import static edu.filonenko.repository.SqlHelper.GENDER_FIELD_NAME;
import static edu.filonenko.repository.SqlHelper.ID_FIELD_NAME;
import static edu.filonenko.repository.SqlHelper.LAST_NAME_FIELD_NAME;
import static edu.filonenko.repository.SqlHelper.SELECT_USERS_SQL;
import static edu.filonenko.repository.SqlHelper.SELECT_USER_BY_ID_SQL;
import static edu.filonenko.repository.SqlHelper.UPDATE_USER_SQL;

/**
 * @author Yulia Filonenko
 **/

@Repository
@Qualifier("userDao")
public class UserJdbcTemplate implements UserDao {

    private static final RowMapper<User> USER_ROW_MAPPER =
            (rs, i) -> new User(
                rs.getLong(ID_FIELD_NAME),
                rs.getString(FIRST_NAME_FIELD_NAME),
                rs.getString(LAST_NAME_FIELD_NAME),
                rs.getString(EMAIL_FIELD_NAME),
                Optional.of(rs.getString(GENDER_FIELD_NAME)).map(Gender::getGenderByName).orElse(null),
                Optional.ofNullable(rs.getDate(BIRTH_DATE_FIELD_NAME)).map(Date::toLocalDate).orElse(null),
                rs.getDouble(BALANCE_FIELD_NAME)
            );

    private static BiFunction<User, Boolean, UnaryOperator<PreparedStatement>> PREPARED_STATEMENT_SETTER = (user, update) -> ps -> {
        int index = 0;
        try {
            ps.setString(++index, user.getFirstName());
            ps.setString(++index, user.getLastName());
            ps.setString(++index, user.getEmail());
            ps.setDate(++index, Optional.ofNullable(user.getBirthDate()).map(Date::valueOf).orElse(null));
            ps.setString(++index, Optional.ofNullable(user.getGender()).map(Gender::getName).orElse(null));
            ps.setDouble(++index, user.getBalance().isInfinite() ? 0 : user.getBalance());
            if(update)
                ps.setLong(++index, user.getUserId());
        } catch (SQLException e) {
            throw new DbException("Cannot populate prepared statement", e);
        }
        return ps;
    };

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public User createUser(User user) throws DbException {
        Optional.ofNullable(user).orElseThrow(() -> new IllegalArgumentException("Entity cannot be null"));

        KeyHolder key = new GeneratedKeyHolder();

        this.jdbcTemplate.update(connection -> PREPARED_STATEMENT_SETTER.apply(user, false)
                    .apply(connection.prepareStatement(CREATE_USER_SQL, Statement.RETURN_GENERATED_KEYS)), key);

        user.setUserId(key.getKey().longValue());

        return user;
    }

    @Override
    public List<User> userList() {
        System.out.println("Dao userList");
        return jdbcTemplate.query(SELECT_USERS_SQL, USER_ROW_MAPPER);
    }

    @Override
    public void editUser(User user) {
        Optional.ofNullable(user).orElseThrow(() -> new IllegalArgumentException("Entity cannot be null"));

        this.jdbcTemplate.update(connection -> PREPARED_STATEMENT_SETTER.apply(user, true)
                .apply(connection.prepareStatement(UPDATE_USER_SQL)));
    }

    @Override
    public User findUser(Long id) {
        List<User> userList = jdbcTemplate.query(SELECT_USER_BY_ID_SQL, new Object[]{id}, USER_ROW_MAPPER);
        return userList.size() == 1 ? userList.get(0) : null;
    }

    @Override
    public void deleteUser(Long id) {
        jdbcTemplate.update(DELETE_USER_BY_ID_SQL, id);
    }
}
