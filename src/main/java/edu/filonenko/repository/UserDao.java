package edu.filonenko.repository;

import edu.filonenko.DbException;
import edu.filonenko.entity.User;

import java.util.List;

/**
 * @author Yulia Filonenko
 **/
public interface UserDao {

    User createUser(User user) throws DbException;

    void editUser(User user) throws DbException;

    User findUser(Long id) throws DbException;

    void deleteUser(Long i) throws DbException;

    List<User> userList() throws DbException;

}
