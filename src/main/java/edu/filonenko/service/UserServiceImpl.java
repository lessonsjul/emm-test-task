package edu.filonenko.service;

import edu.filonenko.entity.User;
import edu.filonenko.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Yulia Filonenko
 **/

@Service("userService")
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void createOrUpdateUser(User user) {
        if (user.getUserId() == null)
            userDao.createUser(user);
        else
            userDao.editUser(user);
    }

    @Override
    public void deleteUser(long userId) {
        System.out.println("UserService userId " + userId);
        userDao.deleteUser(userId);
    }

    @Override
    public User findUser(long userId) {
        return userDao.findUser(userId);
    }

    @Override
    public List<User> userList() {
        System.out.println("UserList");
        return userDao.userList();
    }
}
