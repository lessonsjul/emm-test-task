package edu.filonenko.service;

import edu.filonenko.entity.User;

import java.util.List;

/**
 * @author Yulia Filonenko
 **/
public interface UserService {

    void createOrUpdateUser(User user);

    void deleteUser(long userId);

    User findUser(long userId);

    List<User> userList();
}
