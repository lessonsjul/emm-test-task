package edu.filonenko.entity;

import java.util.Optional;

/**
 * @author Yulia Filonenko
 **/
public enum Gender {
    MALE, FEMALE;


    public int getId() {
        return this.ordinal();
    }

    public String getName() {
        return this.name().toLowerCase();
    }

    public static Gender getGenderByName(String name) {
        try {return Optional.of(name).map(String::toUpperCase).map(Gender::valueOf).get(); }
        catch (Exception e) { return null;}
    }
}
