package edu.filonenko.utils;


import edu.filonenko.entity.Gender;
import edu.filonenko.entity.User;
import edu.filonenko.form.UserForm;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;


/**
 * @author Yulia Filonenko
 **/
public class Utils {

    private static final String DATE_PATTER = "dd-MM-yyyy";
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTER);
    private static final String EMAIL_PATTERN =
            "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

    public static final Function<String, Boolean> CHECK_IS_STRING_EMPTY = stringValue ->
            Optional.ofNullable(stringValue).map(String::trim).map(String::isEmpty).orElse(true);

    public static final Function<String, Boolean> CHECK_EMAIL_FORMAT_CORRECTNESS = emailValue ->
            !CHECK_IS_STRING_EMPTY.apply(emailValue) && Pattern.compile(EMAIL_PATTERN).matcher(emailValue).matches();

    public static final Function<String, Boolean> CHECK_DATE_FORMAT_CORRECTNESS = dateValue -> {
        boolean valid;
        try { valid = Objects.nonNull(toLocalDate(dateValue)); }
        catch(Exception e){valid = false; }
        return valid;
    };

    public static final Function<Integer, Boolean> CHECK_GENDER_CORRECTNESS = genderValue -> genderValue >=0 && genderValue < Gender.values().length;

    public static final Function<String, Boolean> CHECK_BALANCE_FORMAT = balanceValue -> {
        boolean valid;
        try {
            Double value = Optional.ofNullable(balanceValue)
                    .map(String::trim)
                    .map(Double::valueOf)
                    .orElse(null);
            valid = Objects.nonNull(value);
        } catch(Exception e){valid = false; }
        return valid;
    };


    public static String toString(LocalDate dateValue) {
        return DATE_FORMATTER.format(dateValue);
    }

    public static LocalDate toLocalDate(String stringDateValue) {
        return Optional.ofNullable(stringDateValue).map(value -> LocalDate.parse(value, DATE_FORMATTER)).orElse(null);
    }

    public static Integer birthDateToAge(LocalDate birthDate) {
        return Period.between(birthDate, LocalDate.now()).getYears();
    }

    public static User saveUserFormDataIntoUser(UserForm userForm) {
        User user = new User();
        saveUserFormDataIntoUser(user, userForm);
        return user;
    }

    public static void saveUserFormDataIntoUser(User user, UserForm userForm) {
        user.setUserId(userForm.getUserId());
        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        user.setEmail(userForm.getEmail());
        user.setGender(Gender.values()[userForm.getGenderId()]);
        user.setBirthDate(Utils.toLocalDate(userForm.getBirthDate()));
        user.setBalance(Double.valueOf(userForm.getBalanceInput()));
    }

    public static UserForm saveUserDataIntoUserForm(User userById) {
        UserForm userForm = new UserForm();
        saveUserDataIntoUserForm(userForm, userById);
        return userForm;
    }

    public static void saveUserDataIntoUserForm(UserForm userForm, User userById) {
        userForm.setUserId(userById.getUserId());
        userForm.setFirstName(userById.getFirstName());
        userForm.setLastName(userById.getLastName());
        userForm.setEmail(userById.getEmail());
        userForm.setGenderId(Optional.ofNullable(userById.getGender()).map(Gender::getId).orElse(-1));
        userForm.setGenderName(Optional.ofNullable(userById.getGender()).map(Gender::getName).orElse(""));
        userForm.setBirthDate(Utils.toString(userById.getBirthDate()));
        userForm.setAge(Optional.ofNullable(userById.getBirthDate()).map(Utils::birthDateToAge).orElse(0));
        userForm.setBalanceInput(String.format(Locale.US, "%1$.4f", userById.getBalance()));
        userForm.setBalance(userById.getBalance());
    }
}
