package edu.filonenko;

public class DbException extends RuntimeException {

    public DbException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
