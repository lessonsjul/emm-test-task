package edu.filonenko.action;

import edu.filonenko.form.UserForm;
import edu.filonenko.service.UserService;
import edu.filonenko.utils.Utils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.springframework.web.struts.ActionSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static edu.filonenko.action.ForwardValues.FAILURE;
import static edu.filonenko.action.ForwardValues.SUCCESS;

/**
 * @author Yulia Filonenko
 **/
public class ShowUserList extends ActionSupport {

   @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ForwardValues target = SUCCESS;

        List<UserForm> userFormList = null;
        try {
            userFormList =
                    getWebApplicationContext().getBean(UserService.class)
                    .userList().stream()
                    .map(Utils::saveUserDataIntoUserForm)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            target = FAILURE;
            ActionErrors errors = new ActionErrors();
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(e.getMessage()));
            saveMessages(request, errors);
        }

        request.setAttribute("usersList", Optional.ofNullable(userFormList).orElse(new ArrayList<>()));

        return mapping.findForward(target.toString());
    }

}
