package edu.filonenko.action;

import edu.filonenko.entity.User;
import edu.filonenko.form.UserForm;
import edu.filonenko.service.UserService;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.springframework.web.struts.ActionSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static edu.filonenko.action.ForwardValues.SUCCESS;
import static edu.filonenko.utils.Utils.saveUserFormDataIntoUser;

/**
 * @author Yulia Filonenko
 **/
public class SaveOrUpdateUser extends ActionSupport {

   @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        UserForm userForm = (UserForm) form;
        try {
            System.out.println("UserForm data" + userForm);
            User user = saveUserFormDataIntoUser(userForm);
            getWebApplicationContext().getBean(UserService.class).createOrUpdateUser(user);
        } catch (Exception e) {}

        return new ActionRedirect(mapping.findForward(SUCCESS.toString()).getPath());
    }


}
