package edu.filonenko.action;

import edu.filonenko.entity.Gender;
import edu.filonenko.entity.User;
import edu.filonenko.form.UserForm;
import edu.filonenko.service.UserService;
import edu.filonenko.utils.Utils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.web.struts.ActionSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;
import java.util.Optional;

import static edu.filonenko.action.ForwardValues.SUCCESS;

/**
 * @author Yulia Filonenko
 **/

public class GetUserData extends ActionSupport {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setAttribute("genderList", Gender.values());

        UserForm userForm = (UserForm) form;
        UserService userService = getWebApplicationContext().getBean(UserService.class);
        String userId = Optional.ofNullable(request.getParameter("userId")).orElse("");
        if(!userId.isEmpty() && userId.matches("\\d*")) {
            User userById = userService.findUser(Long.valueOf(userId));
            if (Objects.nonNull(userById))
                Utils.saveUserDataIntoUserForm(userForm, userById);
        }

        return mapping.findForward(SUCCESS.toString());
    }
}
