package edu.filonenko.action;

import edu.filonenko.service.UserService;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.springframework.web.struts.ActionSupport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

import static edu.filonenko.action.ForwardValues.FAILURE;
import static edu.filonenko.action.ForwardValues.SUCCESS;

/**
 * @author Yulia Filonenko
 **/

public class DeleteUser extends ActionSupport {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ForwardValues target = SUCCESS;

        String userId = Optional.ofNullable(request.getParameter("userId")).orElse("");
        if(userId.matches("\\d*")) {
            getWebApplicationContext().getBean("userService", UserService.class).deleteUser(Long.valueOf(userId));
        } else {
            target = FAILURE;
            ActionErrors errors = new ActionErrors();
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("error.delete.user"));
            saveMessages(request, errors);
        }


        return new ActionForward(mapping.findForward(target.toString()).getPath(), true);
    }
}
