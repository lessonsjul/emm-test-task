package edu.filonenko.action;

/**
 * @author Yulia Filonenko
 **/
public enum ForwardValues {
    SUCCESS, FAILURE;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
