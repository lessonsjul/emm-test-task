package edu.filonenko.form;

import edu.filonenko.utils.Utils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import javax.servlet.http.HttpServletRequest;

import static edu.filonenko.utils.Utils.CHECK_BALANCE_FORMAT;
import static edu.filonenko.utils.Utils.CHECK_DATE_FORMAT_CORRECTNESS;
import static edu.filonenko.utils.Utils.CHECK_EMAIL_FORMAT_CORRECTNESS;
import static edu.filonenko.utils.Utils.CHECK_GENDER_CORRECTNESS;
import static edu.filonenko.utils.Utils.CHECK_IS_STRING_EMPTY;


/**
 * @author Yulia Bycheva
 **/
public class UserForm extends ActionForm {

    private Long userId;
    private String firstName;
    private String lastName;
    private String email;
    private String birthDate;
    private Integer age;
    private int genderId = -1;
    private String genderName;
    private Double balance;
    private String balanceInput;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getGenderId() {
        return genderId;
    }

    public void setGenderId(int genderId) {
        this.genderId = genderId;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getBalanceInput() {
        return balanceInput;
    }

    public void setBalanceInput(String balanceInput) {
        this.balanceInput = balanceInput;
    }

    @Override
    public String toString() {
        return "UserForm{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", birthDate=" + birthDate +
                ", gender='" + genderId + '\'' +
                ", balance=" + balance +
                ", balanceInput=" + balanceInput +
                '}';
    }


    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        if(CHECK_IS_STRING_EMPTY.apply(firstName))
            errors.add("firstName", new ActionMessage("error.field.mandatory"));

        if(CHECK_IS_STRING_EMPTY.apply(lastName))
            errors.add("lastName", new ActionMessage("error.field.mandatory"));

        if(!CHECK_EMAIL_FORMAT_CORRECTNESS.apply(email))
            errors.add("email", new ActionMessage("error.field.invalid.format"));

        if(!CHECK_DATE_FORMAT_CORRECTNESS.apply(birthDate)) {
            errors.add("birthDate", new ActionMessage("error.field.invalid.format"));
            errors.add("dateFormat", new ActionMessage("info.date.format.required"));
        } else if(Utils.birthDateToAge(Utils.toLocalDate(birthDate)) < 17)
            errors.add("age", new ActionMessage("error.age"));

        if(!CHECK_GENDER_CORRECTNESS.apply(genderId))
            errors.add("gender", new ActionMessage("error.field.mandatory"));

        if(!CHECK_BALANCE_FORMAT.apply(balanceInput)) {
            errors.add("balance", new ActionMessage("error.field.invalid.format"));
            errors.add("balanceFormat", new ActionMessage("info.money.format.required"));
        } else
            this.balance = Double.valueOf(balanceInput);

        return errors;
    }


}
