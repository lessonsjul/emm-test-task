package edu.filonenko.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author Yulia Filonenko
 **/

@Configuration
@ComponentScan(basePackages = "edu.filonenko")
@PropertySource("classpath:settings.properties")
public class AppConfig {

    private static final String DRIVER_CLASS_NAME = "jdbc.driverClassName";
    private static final String DB_URL = "database.url";
    private static final String DB_USERNAME = "database.username";
    private static final String DB_PASSWORD = "database.password";

    @Autowired
    private Environment env;

    @Bean("dataSource")
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty(DRIVER_CLASS_NAME));
        dataSource.setUrl(env.getProperty(DB_URL));
        dataSource.setUsername(env.getProperty(DB_USERNAME));
        dataSource.setPassword(env.getProperty(DB_PASSWORD));

        Properties properties = new Properties();
        properties.setProperty("useUnicode", "true");
        properties.setProperty("useSSL", "false");
        properties.setProperty("useJDBCCompliantTimezoneShift", "true");
        properties.setProperty("useLegacyDatetimeCode", "false");
        properties.setProperty("serverTimezone", "UTC");
//        properties.setProperty("nullNamePatternMatchesAll", "true");
        dataSource.setConnectionProperties(properties);

        return dataSource;
    }

    @Bean("jdbcTemplate")
    public JdbcTemplate jdbcTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        return jdbcTemplate;
    }
}
