CREATE USER IF NOT EXISTS 'emm_admin' IDENTIFIED BY '123456';
CREATE schema if not exists emm;

USE emm;
GRANT ALL PRIVILEGES ON emm.* TO 'emm_admin';
SHOW GRANTS FOR 'emm_admin';

DROP TABLE IF EXISTS users;

CREATE TABLE users (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(254),
    birth_date DATE NOT NULL,
    gender enum('male', 'female'),
    balance double DEFAULT 0.0000
) DEFAULT CHARACTER SET 'utf8';