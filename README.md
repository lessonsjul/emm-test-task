#EMM test task

Web-приложение предоставляет возможность управления базой данных клиентов банка с возможностью добавления, просмотра и удаления.

Сборка проекта осуществляется с помощью maven

1. Для создания базы данных используйте скрипт `db_creation_script.sql` из папки `scripts`
2. Для создания war файла проекта используйте скрипт `build.bat` или `buildmac.sh` из папки `scripts`
######war destination - `target/EmmTask.war`
######DB credential - `src/main/java/resources/settings.properties`
